package com.mcarvalho.musicapp.repository

import com.mcarvalho.musicapp.model.Band

interface BandRepository {
    suspend fun getAll(): List<Band>
}