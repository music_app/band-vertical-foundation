package com.mcarvalho.musicapp.repository

import com.mcarvalho.musicapp.model.Band
import com.mcarvalho.musicapp.model.Genre
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import java.time.LocalDate

class BandRepositoryInMemoryWithMutexImpl : BandRepository {
    private var nextId = 1L
    private val bands = mutableListOf(
        Band(
            id = 1,
            name = "Dead Fish",
            created = LocalDate.of(1991,1,1),
            genre = Genre.HARDCORE),
        Band(
            id = 2,
            name = "Pense",
            created = LocalDate.of(2007,1,1),
            genre = Genre.HARDCORE),
        Band(
            id = 3,
            name = "Charlie Brown Jr",
            created = LocalDate.of(1992,1,1),
            genre = Genre.HARDCORE)
    )
    private val mutex = Mutex()

    override suspend fun getAll(): List<Band> {
        mutex.withLock {
            return bands.reversed()
        }
    }
}