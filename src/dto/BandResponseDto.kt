package com.mcarvalho.musicapp.dto

import com.mcarvalho.musicapp.model.Band
import com.mcarvalho.musicapp.model.Genre
import java.time.LocalDate

data class BandResponseDto (
    val id: Long,
    val name: String,
    val created: LocalDate,
    val genre: Genre
) {
    companion object {
        fun fromModel(band: Band) = BandResponseDto(
            id = band.id,
            name = band.name,
            created = band.created,
            genre = band.genre
        )
    }
}