package com.mcarvalho.musicapp.router

import com.mcarvalho.musicapp.dto.BandResponseDto
import com.mcarvalho.musicapp.repository.BandRepository
import io.ktor.application.call
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.routing.route
import org.kodein.di.generic.instance
import org.kodein.di.ktor.kodein

fun Routing.bands() {
    route("/api/v1/bands") {
        val repo by kodein().instance<BandRepository>()
        get {
            val response = repo.getAll().map { BandResponseDto.fromModel(it) }
            call.respond(response)
        }
    }
}