package com.mcarvalho.musicapp.model

import java.time.LocalDate

enum class Genre{
    CLASSIC_ROCK,
    POP,
    INDIE,
    METAL,
    FOLK,
    COUNTRY,
    PUNK,
    HARDCORE,
    DANCE,
    ELETRO
}

open class Band (
    val id: Long,
    val name: String,
    val created: LocalDate,
    val genre: Genre
)